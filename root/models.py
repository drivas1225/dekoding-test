from django.db import models

# Create your models here.
class Dog(models.Model):
    name = models.CharField(max_length =100, blank = True, null = True)
    breed = models.CharField(max_length =100, blank = True, null = True)
    age = models.IntegerField()
    owner = models.CharField(max_length =100, blank = True, null = True)

#definir Unicode  python 3
    def __str__(self):
        return self.name
