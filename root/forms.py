from django import forms
from django.forms import ModelForm, TextInput, NumberInput, CharField, IntegerField, ModelChoiceField
from .models import Dog



class DogForm(ModelForm):

    class Meta:
        model = Dog
        fields = ('name', 'age', 'breed', 'owner')
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Name', 'class': 'form-control'}),
            'age': NumberInput(attrs={'placeholder': 'Age', 'class': 'form-control', 'min': '0'}),
            'breed': TextInput(attrs={'placeholder': 'Breed', 'class': 'form-control'}),
            'owner' : TextInput(attrs={'placeholder': 'Owner', 'class': 'form-control'})
        }
