from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponseRedirect

from .models import Dog
from .forms import DogForm

class IndexView(View):
    template_name = "index.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class FrontendView(View):
    template_name = "frontend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class BackendView(View):
    template_name = "backend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)

class DogListView(View):
    template_name = "dog/list.html"

    def get(self, request):
        dogs=Dog.objects.all()
        context = {'dogs': dogs}

        return render(request, self.template_name, context)

class DogAddView(View):
    template_name= "dog/add.html"
    def get(self,request):
        form=DogForm()
        return render(request,self.template_name,{'form':form})
    def post(self,request,**kwargs):
        form=DogForm(request.POST)
        if form.is_valid():
            dog=form.save(commit=False)
            dog.save()
            return HttpResponseRedirect('/dog/list/')
